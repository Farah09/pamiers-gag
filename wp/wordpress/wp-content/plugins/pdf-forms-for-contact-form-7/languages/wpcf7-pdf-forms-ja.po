# Copyright (C) 2017 PDF Forms Filler for Contact Form 7
# This file is distributed under the same license as the PDF Forms Filler for Contact Form 7 package.
msgid ""
msgstr ""
"Project-Id-Version: PDF Forms Filler for Contact Form 7 1.0.2\n"
"Report-Msgid-Bugs-To: https://wordpress.org/support/plugin/pdf-forms-for-"
"contact-form-7/\n"
"POT-Creation-Date: 2019-02-12 11:00:47+00:00\n"
"PO-Revision-Date: \n"
"Last-Translator: \n"
"Language-Team: \n"
"Language: ja_JP\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Poedit 2.1.1\n"
"X-Poedit-SourceCharset: UTF-8\n"

#: modules/pdf-ninja.php:40
msgid "Pdf.Ninja API"
msgstr "Pdf.Ninja API"

#: modules/pdf-ninja.php:94 modules/pdf-ninja.php:943
msgid ""
"Failed to get the Pdf.Ninja API key on last attempt.  Please retry manually."
msgstr ""
"最後の試行でPdf.Ninja APIキーを取得できませんでした。 手動で再試行してくださ"
"い。"

#: modules/pdf-ninja.php:122
msgid "Failed to determine the current user."
msgstr "現在のユーザーを特定できませんでした。"

#: modules/pdf-ninja.php:126
msgid "Failed to determine the current user's email address."
msgstr "現在のユーザーの電子メールアドレスを特定できませんでした。"

#: modules/pdf-ninja.php:249
msgid "PHP version 5.3 or higher is required."
msgstr "PHPのバージョン5.3以上が必要です。"

#: modules/pdf-ninja.php:255
msgid "Windows platform is not supported."
msgstr "Windowsプラットフォームはサポートされていません。"

#: modules/pdf-ninja.php:261
msgid "PHP safe mode is not supported."
msgstr "PHPセーフモードはサポートされていません。"

#: modules/pdf-ninja.php:269
msgid "PHP execute functions (exec, proc_open, proc_close) are disabled."
msgstr "PHPの実行関数（exec、proc_open、proc_close）は無効になっています。"

#: modules/pdf-ninja.php:291
msgid "Chroot environments are not supported"
msgstr "chroot環境はサポートされていません"

#: modules/pdf-ninja.php:299
msgid "Required binaries are not available on this platform."
msgstr "必要なバイナリはこのプラットフォームでは使用できません。"

#: modules/pdf-ninja.php:317 modules/pdf-ninja.php:375
msgid "Failed to get API server response"
msgstr "APIサーバからの応答を取得できませんでした"

#: modules/pdf-ninja.php:326 modules/pdf-ninja.php:384
msgid "Failed to decode API server response"
msgstr "APIサーバーの応答をデコードできませんでした"

#: modules/pdf-ninja.php:334 modules/pdf-ninja.php:392
msgid "Failed to download a file from the API server"
msgstr "APIサーバーからファイルをダウンロードできませんでした"

#: modules/pdf-ninja.php:419 modules/pdf-ninja.php:547
#: modules/pdf-ninja.php:566 modules/pdf-ninja.php:608
#: modules/pdf-ninja.php:722 modules/pdf-ninja.php:752
msgid "Pdf.Ninja API server did not send an expected response"
msgstr "Pdf.NinjaのAPIサーバが期待される応答を送信しませんでした"

#: modules/pdf-ninja.php:473 wpcf7-pdf-forms.php:795 wpcf7-pdf-forms.php:1218
msgid "File not found"
msgstr "ファイルがありません"

#: modules/pdf-ninja.php:613 modules/pdf-ninja.php:727
#: modules/pdf-ninja.php:757 wpcf7-pdf-forms.php:467
msgid "Failed to open file for writing"
msgstr "書き込みのためにファイルを開くことができませんでした"

#: modules/pdf-ninja.php:619 modules/pdf-ninja.php:733
#: modules/pdf-ninja.php:763 wpcf7-pdf-forms.php:473
msgid "Failed to create file"
msgstr "ファイルの作成に失敗した"

#: modules/pdf-ninja.php:657 modules/pdf-ninja.php:661
msgid "Failed to encode JSON data"
msgstr "JSONデータのエンコードに失敗しました"

#: modules/pdf-ninja.php:798 wpcf7-pdf-forms.php:729 wpcf7-pdf-forms.php:1076
#: wpcf7-pdf-forms.php:1260
msgid "Permission denied"
msgstr "アクセス拒否"

#: modules/pdf-ninja.php:855
msgid ""
"This service provides functionality for working with PDF files via a web API."
msgstr ""
"このサービスは、Web APIを介してPDFファイルを操作するための機能を提供します。"

#: modules/pdf-ninja.php:856 modules/pdf-ninja.php:895
msgid "API Key"
msgstr "APIキー"

#: modules/pdf-ninja.php:858
msgid "copy key"
msgstr "コピーキー"

#: modules/pdf-ninja.php:859
msgid "copied!"
msgstr "コピーしました！"

#: modules/pdf-ninja.php:860 modules/pdf-ninja.php:897
msgid "API URL"
msgstr "API URL"

#: modules/pdf-ninja.php:862 modules/pdf-ninja.php:899
msgid "Data Security"
msgstr "データセキュリティ"

#: modules/pdf-ninja.php:863 modules/pdf-ninja.php:900
msgid "Ignore certificate verification errors"
msgstr "証明書検証エラーを無視する"

#: modules/pdf-ninja.php:865 modules/pdf-ninja.php:902
msgid ""
"Warning: Using plain HTTP or disabling certificate verification can lead to "
"data leaks."
msgstr ""
"警告：プレーンHTTPを使用するか、証明書の検証を無効にすると、データが漏洩する"
"可能性があります。"

#: modules/pdf-ninja.php:866
msgid "Enterprise Extension"
msgstr "エンタープライズエクステンション"

#: modules/pdf-ninja.php:867
msgid "Extension is supported."
msgstr "拡張機能がサポートされています。"

#: modules/pdf-ninja.php:867
msgid "Extension is not supported."
msgstr "拡張機能はサポートされていません。"

#: modules/pdf-ninja.php:868 modules/pdf-ninja.php:881
msgid "Edit"
msgstr "編集"

#: modules/pdf-ninja.php:879
msgid "Error!"
msgstr "エラーが発生しました！"

#: modules/pdf-ninja.php:894
msgid "The following form allows you to edit your API key."
msgstr "以下のフォームはあなたのAPIキーを編集することができます。"

#: modules/pdf-ninja.php:905
msgid "Save"
msgstr "保存す"

#: modules/pdf-ninja.php:906
msgid "Get New Key"
msgstr "新しいキーを取得します"

#: modules/pdf-ninja.php:917 modules/pdf-ninja.php:923
msgid "PDF Forms for CF7 plugin error"
msgstr "CF7プラグインエラーのPDFフォーム"

#: modules/pdf-ninja.php:918
msgid "Can't save new key."
msgstr "新しいキーを保存することはできません。"

#: modules/pdf-ninja.php:929
msgid "Settings saved."
msgstr "保存された設定。"

#: modules/pdf-ninja.php:942 wpcf7-pdf-forms.php:168
msgid "PDF Forms Filler for CF7 plugin error"
msgstr "PDFフォームフィラーCF7プラグイン・エラーのために"

#: modules/pdf-ninja.php:958
msgid ""
"Warning: Your integration settings indicate you are using an insecure "
"connection to the Pdf.Ninja API server."
msgstr ""
"警告：あなたの統合設定は、Pdf.Ninja APIサーバーへの安全でない接続を使用してい"
"ることを示しています。"

#: modules/service.php:7 modules/service.php:12 modules/service.php:17
#: modules/service.php:22 modules/service.php:27
msgid "Missing feature"
msgstr "不足している機能"

#: wpcf7-pdf-forms.php:155
msgid "Tutorial Video"
msgstr "チュートリアルビデオ"

#: wpcf7-pdf-forms.php:156
msgid "Support"
msgstr "サポート"

#: wpcf7-pdf-forms.php:169
msgid "The required plugin 'Contact Form 7' is not installed!"
msgstr "必要なプラグイン「お問い合わせフォーム7」インストールされていません！"

#: wpcf7-pdf-forms.php:226
msgid "File not specified"
msgstr "ファイルを指定しません"

#: wpcf7-pdf-forms.php:227
msgid "Unknown error"
msgstr "不明なエラー"

#: wpcf7-pdf-forms.php:228
msgid "Please copy/paste tags manually"
msgstr "手動でタグをコピー/貼り付けてください"

#: wpcf7-pdf-forms.php:229
msgid ""
"Are you sure you want to delete this file?  This will delete field mappings "
"and image embeds associated with this file."
msgstr ""
"このファイルを削除してもよろしいですか？ これにより、このファイルに関連付けら"
"れているフィールドのマッピングとイメージの埋め込みが削除されます。"

#: wpcf7-pdf-forms.php:230
msgid "Are you sure you want to delete this mapping?"
msgstr "このマッピングを削除してもよろしいですか？"

#: wpcf7-pdf-forms.php:231
msgid "Are you sure you want to delete this embeded image?"
msgstr "この埋め込み画像を削除してもよろしいですか？"

#: wpcf7-pdf-forms.php:232 wpcf7-pdf-forms.php:1389
msgid "Show Help"
msgstr "ヘルプを表示"

#: wpcf7-pdf-forms.php:233 wpcf7-pdf-forms.php:1390
msgid "Hide Help"
msgstr "ヘルプを隠す"

#: wpcf7-pdf-forms.php:254
msgid "PDF Forms"
msgstr "PDFフォーム"

#: wpcf7-pdf-forms.php:460
msgid "Failed to download file"
msgstr "ファイルのダウンロードに失敗しました"

#: wpcf7-pdf-forms.php:687
msgid ""
"Error generating PDF: {error-message} at {error-file}:{error-line}\n"
"\n"
"Form data:\n"
"\n"
msgstr ""
"{error-file}：{error-line}でPDFの生成中にエラーが発生しました：{error-"
"message}\n"
"\n"
"フォームデータ：\n"
"\n"

#: wpcf7-pdf-forms.php:726 wpcf7-pdf-forms.php:959 wpcf7-pdf-forms.php:1067
#: wpcf7-pdf-forms.php:1171 wpcf7-pdf-forms.php:1251
msgid "Nonce mismatch"
msgstr "ノンス不一致"

#: wpcf7-pdf-forms.php:736
msgid "Invalid file type, must be a PDF file"
msgstr "無効なファイルタイプです。PDFファイルでなければなりません"

#: wpcf7-pdf-forms.php:833
msgid "No service"
msgstr "サービス無し"

#: wpcf7-pdf-forms.php:977
msgid ""
"This PDF file does not appear to contain a PDF form.  See https://acrobat."
"adobe.com/us/en/acrobat/how-to/create-fillable-pdf-forms-creator.html for "
"more information."
msgstr ""
"このPDFファイルにはPDFフォームが含まれていません。 詳細については、https：//"
"acrobat.adobe.com/us/en/acrobat/how-to/create-fillable-pdf-forms-creator.html"
"を参照してください。"

#: wpcf7-pdf-forms.php:1073
msgid "Invalid post ID"
msgstr "無効なポストID"

#: wpcf7-pdf-forms.php:1142
msgid "Failed to get Contact Form fields"
msgstr "連絡先フォームのフィールドを取得できませんでした"

#: wpcf7-pdf-forms.php:1257
msgid "Invalid page number"
msgstr "ページ番号が無効です"

#: wpcf7-pdf-forms.php:1266
msgid "Failed to retrieve page snapshot"
msgstr "ページスナップショットの取得に失敗しました"

#: wpcf7-pdf-forms.php:1292 wpcf7-pdf-forms.php:1301
msgid "PDF Form"
msgstr "PDFフォーム"

#: wpcf7-pdf-forms.php:1351
msgid ""
"You can use this tag generator to attach a PDF file to your form, insert "
"tags into your form and link them to fields in the PDF file.  You can also "
"embed images (generated by other Contact Form 7 tags or fields) into the PDF "
"file.  Changes here are applied when the contact form is saved."
msgstr ""
"このタグジェネレータを使用して、フォームにPDFファイルを添付し、フォームにタグ"
"を挿入してPDFファイルのフィールドにリンクすることができます。 他のContact "
"Form 7のタグやフィールドで生成された画像をPDFファイルに埋め込むこともできま"
"す。 連絡先フォームが保存されると、ここでの変更が適用されます。"

#: wpcf7-pdf-forms.php:1352
msgid "Upload & Attach a PDF File"
msgstr "PDFファイルのアップロード＆取り付け"

#: wpcf7-pdf-forms.php:1353
msgid "Insert Tags"
msgstr "挿入タグ"

#: wpcf7-pdf-forms.php:1354
msgid "Insert and Link"
msgstr "挿入とリンク"

#: wpcf7-pdf-forms.php:1355
msgid ""
"This button allows you to generate tags for all remaining unlinked PDF "
"fields, insert them into the form and link them to their corresponding "
"fields."
msgstr ""
"このボタンを使用すると、リンクされていないすべてのPDFフィールドのタグを生成"
"し、フォームに挿入して対応するフィールドにリンクすることができます。"

#: wpcf7-pdf-forms.php:1356
msgid "Insert & Link All"
msgstr "すべてを挿入してリンクする"

#: wpcf7-pdf-forms.php:1357 wpcf7-pdf-forms.php:1376
msgid "Delete"
msgstr "削除"

#: wpcf7-pdf-forms.php:1358
msgid "Options"
msgstr "オプション"

#: wpcf7-pdf-forms.php:1359
msgid "Skip when empty"
msgstr "空のときは無視する"

#: wpcf7-pdf-forms.php:1360
msgid "Attach to primary email message"
msgstr "プライマリ電子メールメッセージに添付"

#: wpcf7-pdf-forms.php:1361
msgid "Attach to secondary email message"
msgstr "セカンダリ電子メールメッセージに添付"

#: wpcf7-pdf-forms.php:1362
msgid "Flatten"
msgstr "フラットン"

#: wpcf7-pdf-forms.php:1363
msgid "Filename (mail-tags can be used)"
msgstr "ファイル名（メールタグも使用可能）"

#: wpcf7-pdf-forms.php:1364
msgid "Field Mapper Tool"
msgstr "フィールドマッパーツール"

#: wpcf7-pdf-forms.php:1365
msgid ""
"This tool can be used to link Contact Form 7 fields with fields within the "
"PDF files.  Contact Form 7 fields can also be generated.  When the user "
"submits the form, data from Contact Form 7 fields will be inserted into "
"correspoinding fields in the PDF file."
msgstr ""
"このツールを使用すると、Contact Form 7フィールドをPDFファイル内のフィールドに"
"リンクすることができます。 Contact Form 7フィールドも生成できます。 ユーザー"
"がフォームを送信すると、Contact Form 7フィールドのデータがPDFファイルの対応す"
"るフィールドに挿入されます。"

#: wpcf7-pdf-forms.php:1366
msgid "PDF field"
msgstr "PDFフィールド"

#: wpcf7-pdf-forms.php:1367
msgid "CF7 field"
msgstr "CF7フィールド"

#: wpcf7-pdf-forms.php:1368
msgid "Add Mapping"
msgstr "マッピングの追加"

#: wpcf7-pdf-forms.php:1369
msgid "Delete All"
msgstr "すべて削除"

#: wpcf7-pdf-forms.php:1370
msgid "New Tag:"
msgstr "新しいタグ："

#: wpcf7-pdf-forms.php:1371
msgid "Tag Generator Tool (deprecated)"
msgstr "タグ生成ツール（非推奨）"

#: wpcf7-pdf-forms.php:1372
msgid ""
"This tool allows one to create CF7 fields that are linked to PDF fields by "
"name.  This feature is deprecated in favor of the field mapper tool."
msgstr ""
"このツールを使用すると、PDFフィールドに名前でリンクされたCF7フィールドを作成"
"できます。 このフィーチャーは、フィールドマッパーツールを使用して非推奨になり"
"ました。"

#: wpcf7-pdf-forms.php:1373
msgid "Image Embedding Tool"
msgstr "画像埋め込みツール"

#: wpcf7-pdf-forms.php:1374
msgid ""
"This tool allows embedding images into PDF files.  Images are taken from "
"field attachments or field values that are URLs.  You must select a PDF "
"file, its page and draw a bounding box for image insertion."
msgstr ""
"このツールを使用すると、イメージをPDFファイルに埋め込むことができます。 画像"
"はフィールドの添付ファイルまたはURLであるフィールド値から取得されます。 PDF"
"ファイルとそのページを選択し、画像挿入用のバウンディングボックスを描画する必"
"要があります。"

#: wpcf7-pdf-forms.php:1375
msgid "Embed Image"
msgstr "画像を埋め込む"

#: wpcf7-pdf-forms.php:1377
msgid "PDF file"
msgstr "PDFファイル"

#: wpcf7-pdf-forms.php:1378
msgid "Page"
msgstr "ページ"

#: wpcf7-pdf-forms.php:1379
msgid "Select a region where the image needs to be embeded."
msgstr "イメージを埋め込む必要がある領域を選択します。"

#: wpcf7-pdf-forms.php:1387
msgid ""
"Have a question/comment/problem?  Feel free to use {a-href-forum}the support "
"forum{/a} and view {a-href-tutorial}the tutorial video{/a}."
msgstr ""
"質問/コメント/問題がありますか？ {a-href-forum}サポートフォーラム{/a}を使い、"
"{a-href-tutorial}チュートリアルビデオ{/a}をご覧ください。"

#: wpcf7-pdf-forms.php:1391
msgid "Get Tags"
msgstr "タグを取得"

#: wpcf7-pdf-forms.php:1392
msgid "All PDFs"
msgstr "すべてのPDF"

#: wpcf7-pdf-forms.php:1393
msgid "Return to Form"
msgstr "フォームに戻る"

#: wpcf7-pdf-forms.php:1398
msgid "Your CF7 plugin is too out of date, please upgrade."
msgstr ""
"あなたのCF7プラグインはあまりにも古くなって、アップグレードしてくださいです。"

#. Plugin Name of the plugin/theme
msgid "PDF Forms Filler for Contact Form 7"
msgstr "お問い合わせフォーム7のためのPDFフォームフィラー"

#. Plugin URI of the plugin/theme
msgid "https://github.com/maximum-software/wpcf7-pdf-forms"
msgstr "https://github.com/maximum-software/wpcf7-pdf-forms"

#. Description of the plugin/theme
msgid ""
"Create Contact Form 7 forms from PDF forms.  Get PDF forms filled "
"automatically and attached to email messages upon form submission on your "
"website.  Embed images in PDF files.  Uses Pdf.Ninja API for working with "
"PDF files.  See tutorial video for a demo."
msgstr ""
"PDFフォームからContact Form 7フォームを作成します。 あなたのウェブサイトに"
"フォーム提出時に自動的にPDFフォームが埋め込まれ、メールメッセージに添付されま"
"す。 イメージをPDFファイルに埋め込みます。 PDFファイルの操作にPdf.Ninja APIを"
"使用します。 デモのチュートリアルビデオをご覧ください。"

#. Author of the plugin/theme
msgid "Maximum.Software"
msgstr "Maximum.Software"

#. Author URI of the plugin/theme
msgid "https://maximum.software/"
msgstr "https://maximum.software/"

#~ msgid "Cannot create temporary PDF file"
#~ msgstr "一時的なPDFファイルを作成できません"

#~ msgid "Could not get a Pdf.Ninja API key."
#~ msgstr "Pdf.NinjaのAPIキーを取得できませんでした。"

#~ msgid "How-To"
#~ msgstr "実行する方法"

#~ msgid ""
#~ "Attach a PDF file to your form and insert tags into your form that map to "
#~ "fields in the PDF file."
#~ msgstr ""
#~ "フォームにPDFファイルを添付し、PDFファイル内のフィールドにマップフォームに"
#~ "タグを挿入します。"

#~ msgid "Invalid attachment ID"
#~ msgstr "無効な添付ファイルID"

#~ msgid "Invalid field"
#~ msgstr "無効なフィールド"
