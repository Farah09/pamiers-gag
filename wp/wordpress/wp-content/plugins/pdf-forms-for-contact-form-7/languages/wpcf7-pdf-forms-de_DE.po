# Copyright (C) 2017 PDF Forms Filler for Contact Form 7
# This file is distributed under the same license as the PDF Forms Filler for Contact Form 7 package.
msgid ""
msgstr ""
"Project-Id-Version: PDF Forms Filler for Contact Form 7 1.0.2\n"
"Report-Msgid-Bugs-To: https://wordpress.org/support/plugin/pdf-forms-for-"
"contact-form-7/\n"
"POT-Creation-Date: 2019-02-12 11:00:47+00:00\n"
"PO-Revision-Date: \n"
"Last-Translator: \n"
"Language-Team: \n"
"Language: de_DE\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Poedit 2.1.1\n"
"X-Poedit-SourceCharset: UTF-8\n"

#: modules/pdf-ninja.php:40
msgid "Pdf.Ninja API"
msgstr "Pdf.Ninja API"

#: modules/pdf-ninja.php:94 modules/pdf-ninja.php:943
msgid ""
"Failed to get the Pdf.Ninja API key on last attempt.  Please retry manually."
msgstr ""
"Fehler beim Abrufen des Pdf.Ninja-API-Schlüssels beim letzten Versuch. Bitte "
"versuchen Sie es erneut manuell."

#: modules/pdf-ninja.php:122
msgid "Failed to determine the current user."
msgstr "Fehler beim ermitteln des aktuellen Benutzers."

#: modules/pdf-ninja.php:126
msgid "Failed to determine the current user's email address."
msgstr ""
"Die E-Mail-Adresse des aktuellen Benutzers konnte nicht ermittelt werden."

#: modules/pdf-ninja.php:249
msgid "PHP version 5.3 or higher is required."
msgstr "PHP Version 5.3 oder höher ist erforderlich."

#: modules/pdf-ninja.php:255
msgid "Windows platform is not supported."
msgstr "Windows-Plattform wird nicht unterstützt."

#: modules/pdf-ninja.php:261
msgid "PHP safe mode is not supported."
msgstr "Der PHP-abgesicherte Modus wird nicht unterstützt."

#: modules/pdf-ninja.php:269
msgid "PHP execute functions (exec, proc_open, proc_close) are disabled."
msgstr ""
"PHP-Ausführungsfunktionen (exec, proc_open, proc_close) sind deaktiviert."

#: modules/pdf-ninja.php:291
msgid "Chroot environments are not supported"
msgstr "Chroot-Umgebungen werden nicht unterstützt"

#: modules/pdf-ninja.php:299
msgid "Required binaries are not available on this platform."
msgstr "Erforderliche Binärdateien sind auf dieser Plattform nicht verfügbar."

#: modules/pdf-ninja.php:317 modules/pdf-ninja.php:375
msgid "Failed to get API server response"
msgstr "Fehler beim API-Server Antwort erhalten"

#: modules/pdf-ninja.php:326 modules/pdf-ninja.php:384
msgid "Failed to decode API server response"
msgstr "Fehler beim API-Server-Antwort zu dekodieren"

#: modules/pdf-ninja.php:334 modules/pdf-ninja.php:392
msgid "Failed to download a file from the API server"
msgstr "Fehler beim herunterladen einer Datei aus dem API-server"

#: modules/pdf-ninja.php:419 modules/pdf-ninja.php:547
#: modules/pdf-ninja.php:566 modules/pdf-ninja.php:608
#: modules/pdf-ninja.php:722 modules/pdf-ninja.php:752
msgid "Pdf.Ninja API server did not send an expected response"
msgstr "Pdf.Ninja API-Server nicht eine erwartete Antwort senden"

#: modules/pdf-ninja.php:473 wpcf7-pdf-forms.php:795 wpcf7-pdf-forms.php:1218
msgid "File not found"
msgstr "Datei nicht gefunden"

#: modules/pdf-ninja.php:613 modules/pdf-ninja.php:727
#: modules/pdf-ninja.php:757 wpcf7-pdf-forms.php:467
msgid "Failed to open file for writing"
msgstr "Fehler beim öffnen der Datei zum schreiben"

#: modules/pdf-ninja.php:619 modules/pdf-ninja.php:733
#: modules/pdf-ninja.php:763 wpcf7-pdf-forms.php:473
msgid "Failed to create file"
msgstr "Fehler beim erstellen der Datei"

#: modules/pdf-ninja.php:657 modules/pdf-ninja.php:661
msgid "Failed to encode JSON data"
msgstr "Fehler beim Codieren von JSON-Daten"

#: modules/pdf-ninja.php:798 wpcf7-pdf-forms.php:729 wpcf7-pdf-forms.php:1076
#: wpcf7-pdf-forms.php:1260
msgid "Permission denied"
msgstr "Zugriff verweigert"

#: modules/pdf-ninja.php:855
msgid ""
"This service provides functionality for working with PDF files via a web API."
msgstr ""
"Dieser Dienst stellt Funktionen für die Arbeit mit PDF-Dateien über eine web-"
"API."

#: modules/pdf-ninja.php:856 modules/pdf-ninja.php:895
msgid "API Key"
msgstr "API-Schlüssel"

#: modules/pdf-ninja.php:858
msgid "copy key"
msgstr "Schlüssel kopieren"

#: modules/pdf-ninja.php:859
msgid "copied!"
msgstr "kopiert!"

#: modules/pdf-ninja.php:860 modules/pdf-ninja.php:897
msgid "API URL"
msgstr "API-URL"

#: modules/pdf-ninja.php:862 modules/pdf-ninja.php:899
msgid "Data Security"
msgstr "Datensicherheit"

#: modules/pdf-ninja.php:863 modules/pdf-ninja.php:900
msgid "Ignore certificate verification errors"
msgstr "Ignorieren Sie Zertifikatüberprüfungsfehler"

#: modules/pdf-ninja.php:865 modules/pdf-ninja.php:902
msgid ""
"Warning: Using plain HTTP or disabling certificate verification can lead to "
"data leaks."
msgstr ""
"Warnung: Die Verwendung von reinem HTTP oder nicht überprüftem Zertifikat "
"kann zu Datenlecks führen."

#: modules/pdf-ninja.php:866
msgid "Enterprise Extension"
msgstr "Enterprise Extension"

#: modules/pdf-ninja.php:867
msgid "Extension is supported."
msgstr "Erweiterung wird unterstützt."

#: modules/pdf-ninja.php:867
msgid "Extension is not supported."
msgstr "Erweiterung wird nicht unterstützt."

#: modules/pdf-ninja.php:868 modules/pdf-ninja.php:881
msgid "Edit"
msgstr "Bearbeiten"

#: modules/pdf-ninja.php:879
msgid "Error!"
msgstr "Fehler!"

#: modules/pdf-ninja.php:894
msgid "The following form allows you to edit your API key."
msgstr "Das folgende Formular können Sie Ihre API-Schlüssel bearbeiten."

#: modules/pdf-ninja.php:905
msgid "Save"
msgstr "Sparen"

#: modules/pdf-ninja.php:906
msgid "Get New Key"
msgstr "Get New Key"

#: modules/pdf-ninja.php:917 modules/pdf-ninja.php:923
msgid "PDF Forms for CF7 plugin error"
msgstr "PDF Forms Filler for CF7 Plugin Fehler"

#: modules/pdf-ninja.php:918
msgid "Can't save new key."
msgstr "Es können keine neuen Schlüssel speichern."

#: modules/pdf-ninja.php:929
msgid "Settings saved."
msgstr "Einstellungen gespeichert."

#: modules/pdf-ninja.php:942 wpcf7-pdf-forms.php:168
msgid "PDF Forms Filler for CF7 plugin error"
msgstr "PDF-Formulare Füller für CF7 Plugin-Fehler"

#: modules/pdf-ninja.php:958
msgid ""
"Warning: Your integration settings indicate you are using an insecure "
"connection to the Pdf.Ninja API server."
msgstr ""
"Warnung: Ihre Integrationseinstellungen zeigen an, dass Sie eine unsichere "
"Verbindung zum Pdf.Ninja-API-Server verwenden."

#: modules/service.php:7 modules/service.php:12 modules/service.php:17
#: modules/service.php:22 modules/service.php:27
msgid "Missing feature"
msgstr "Fehlende Funktion"

#: wpcf7-pdf-forms.php:155
msgid "Tutorial Video"
msgstr "Tutorial video"

#: wpcf7-pdf-forms.php:156
msgid "Support"
msgstr "Unterstützung"

#: wpcf7-pdf-forms.php:169
msgid "The required plugin 'Contact Form 7' is not installed!"
msgstr "Das erforderliche Plugin ‚Contact Form 7‘ ist nicht installiert!"

#: wpcf7-pdf-forms.php:226
msgid "File not specified"
msgstr "Datei nicht erlaubt"

#: wpcf7-pdf-forms.php:227
msgid "Unknown error"
msgstr "Unbekannter Fehler"

#: wpcf7-pdf-forms.php:228
msgid "Please copy/paste tags manually"
msgstr "Bitte kopieren Sie die Tags manuell"

#: wpcf7-pdf-forms.php:229
msgid ""
"Are you sure you want to delete this file?  This will delete field mappings "
"and image embeds associated with this file."
msgstr ""
"Sind Sie sicher, dass Sie möchten, um diese Datei zu löschen? Dieses löschen-"
"Feld-mappings und Bild bettet mit dieser Datei verknüpft."

#: wpcf7-pdf-forms.php:230
msgid "Are you sure you want to delete this mapping?"
msgstr "Möchten Sie diese Zuordnung wirklich löschen?"

#: wpcf7-pdf-forms.php:231
msgid "Are you sure you want to delete this embeded image?"
msgstr "Sind Sie sicher, dass Sie löschen möchten dieses eingebettete Bild?"

#: wpcf7-pdf-forms.php:232 wpcf7-pdf-forms.php:1389
msgid "Show Help"
msgstr "Zeig Hilfe"

#: wpcf7-pdf-forms.php:233 wpcf7-pdf-forms.php:1390
msgid "Hide Help"
msgstr "Hilfe ausblenden"

#: wpcf7-pdf-forms.php:254
msgid "PDF Forms"
msgstr "PDF-Formulare"

#: wpcf7-pdf-forms.php:460
msgid "Failed to download file"
msgstr "Fehler beim herunterladen der Datei"

#: wpcf7-pdf-forms.php:687
msgid ""
"Error generating PDF: {error-message} at {error-file}:{error-line}\n"
"\n"
"Form data:\n"
"\n"
msgstr ""
"Fehler beim Erzeugen von PDF: {error-message} bei {error-file}:{error-line}\n"
"\n"
"Formulardaten:\n"
"\n"

#: wpcf7-pdf-forms.php:726 wpcf7-pdf-forms.php:959 wpcf7-pdf-forms.php:1067
#: wpcf7-pdf-forms.php:1171 wpcf7-pdf-forms.php:1251
msgid "Nonce mismatch"
msgstr "Nonce Mismatch"

#: wpcf7-pdf-forms.php:736
msgid "Invalid file type, must be a PDF file"
msgstr "Ungültige Datei-Typ, muss eine PDF-Datei"

#: wpcf7-pdf-forms.php:833
msgid "No service"
msgstr "Kein Service"

#: wpcf7-pdf-forms.php:977
msgid ""
"This PDF file does not appear to contain a PDF form.  See https://acrobat."
"adobe.com/us/en/acrobat/how-to/create-fillable-pdf-forms-creator.html for "
"more information."
msgstr ""
"Diese pdf Datei scheint kein pdf Formular zu enthalten. Weitere "
"Informationen erhältst Du hier: https://acrobat.adobe.com/us/en/acrobat/how-"
"to/create-fillable-pdf-forms-creator.html"

#: wpcf7-pdf-forms.php:1073
msgid "Invalid post ID"
msgstr "Ungültige Post-ID"

#: wpcf7-pdf-forms.php:1142
msgid "Failed to get Contact Form fields"
msgstr "Fehler beim Abrufen der Kontaktformularfelder"

#: wpcf7-pdf-forms.php:1257
msgid "Invalid page number"
msgstr "Ungültige Seitenzahl"

#: wpcf7-pdf-forms.php:1266
msgid "Failed to retrieve page snapshot"
msgstr "Fehler beim abrufen der Seite snapshot"

#: wpcf7-pdf-forms.php:1292 wpcf7-pdf-forms.php:1301
msgid "PDF Form"
msgstr "PDF-Formularname"

#: wpcf7-pdf-forms.php:1351
msgid ""
"You can use this tag generator to attach a PDF file to your form, insert "
"tags into your form and link them to fields in the PDF file.  You can also "
"embed images (generated by other Contact Form 7 tags or fields) into the PDF "
"file.  Changes here are applied when the contact form is saved."
msgstr ""
"Sie können diese tag-generator zum anfügen einer PDF-Datei zu Ihrem "
"Formular, insert-tags in Ihrem Formular, und verknüpfen Sie Sie, um Felder "
"in der PDF-Datei. Sie können auch Bilder einbinden (generiert, die von "
"anderen Contact Form 7-tags oder Felder) in der PDF-Datei. Die änderungen "
"werden angewendet, wenn das Kontakt-Formular gespeichert wird."

#: wpcf7-pdf-forms.php:1352
msgid "Upload & Attach a PDF File"
msgstr "Laden Sie & Sie eine PDF-Datei anhängen"

#: wpcf7-pdf-forms.php:1353
msgid "Insert Tags"
msgstr "Insert-Tags"

#: wpcf7-pdf-forms.php:1354
msgid "Insert and Link"
msgstr "Einfügen und verknüpfen"

#: wpcf7-pdf-forms.php:1355
msgid ""
"This button allows you to generate tags for all remaining unlinked PDF "
"fields, insert them into the form and link them to their corresponding "
"fields."
msgstr ""
"Mit dieser Schaltfläche können Sie \"tags generieren\" für alle "
"verbleibenden nicht verknüpften PDF-Felder, fügen Sie Sie in die form und "
"binden Sie an Ihre entsprechenden Felder ein."

#: wpcf7-pdf-forms.php:1356
msgid "Insert & Link All"
msgstr "Alles einfügen und verknüpfen"

#: wpcf7-pdf-forms.php:1357 wpcf7-pdf-forms.php:1376
msgid "Delete"
msgstr "Löschen"

#: wpcf7-pdf-forms.php:1358
msgid "Options"
msgstr "Optionen"

#: wpcf7-pdf-forms.php:1359
msgid "Skip when empty"
msgstr "Überspringen, wenn leer"

#: wpcf7-pdf-forms.php:1360
msgid "Attach to primary email message"
msgstr "An primäre E-Mail-Nachricht anhängen"

#: wpcf7-pdf-forms.php:1361
msgid "Attach to secondary email message"
msgstr "An sekundäre E-Mail-Nachricht anhängen"

#: wpcf7-pdf-forms.php:1362
msgid "Flatten"
msgstr "Reduzieren"

#: wpcf7-pdf-forms.php:1363
msgid "Filename (mail-tags can be used)"
msgstr "Dateiname (Mail-Tags können verwendet werden)"

#: wpcf7-pdf-forms.php:1364
msgid "Field Mapper Tool"
msgstr "Feld-Mapper-Werkzeug"

#: wpcf7-pdf-forms.php:1365
msgid ""
"This tool can be used to link Contact Form 7 fields with fields within the "
"PDF files.  Contact Form 7 fields can also be generated.  When the user "
"submits the form, data from Contact Form 7 fields will be inserted into "
"correspoinding fields in the PDF file."
msgstr ""
"Dieses tool kann verwendet werden, um link Contact Form 7 Felder, wobei die "
"Felder in den PDF-Dateien. Contact Form 7 Felder können auch generiert "
"werden. Wenn der Benutzer das Formular sendet, werden die Daten von Contact "
"Form 7 Felder eingefügt werden, in die entsprechenden Felder in der PDF-"
"Datei."

#: wpcf7-pdf-forms.php:1366
msgid "PDF field"
msgstr "PDF-Feld"

#: wpcf7-pdf-forms.php:1367
msgid "CF7 field"
msgstr "CF7-Feld"

#: wpcf7-pdf-forms.php:1368
msgid "Add Mapping"
msgstr "Mapping hinzufügen"

#: wpcf7-pdf-forms.php:1369
msgid "Delete All"
msgstr "Alles löschen"

#: wpcf7-pdf-forms.php:1370
msgid "New Tag:"
msgstr "Neuer Tag:"

#: wpcf7-pdf-forms.php:1371
msgid "Tag Generator Tool (deprecated)"
msgstr "Tag Generator Tool (veraltet)"

#: wpcf7-pdf-forms.php:1372
msgid ""
"This tool allows one to create CF7 fields that are linked to PDF fields by "
"name.  This feature is deprecated in favor of the field mapper tool."
msgstr ""
"Dieses Tool ermöglicht es, CF7-Felder zu erstellen, die mit PDF-Feldern nach "
"Namen verknüpft sind. Diese Funktion ist zugunsten des Feldzuordnertools "
"veraltet."

#: wpcf7-pdf-forms.php:1373
msgid "Image Embedding Tool"
msgstr "Bild-Embedding-Tool"

#: wpcf7-pdf-forms.php:1374
msgid ""
"This tool allows embedding images into PDF files.  Images are taken from "
"field attachments or field values that are URLs.  You must select a PDF "
"file, its page and draw a bounding box for image insertion."
msgstr ""
"Dieses tool ermöglicht das einbetten von Grafiken in PDF-Dateien. Bilder "
"stammen aus dem Bereich Anlagen-oder Feld-Werte, die URLs. Sie müssen wählen "
"Sie eine PDF-Datei der Seite und ziehen Sie einen Begrenzungsrahmen für das "
"Bild einfügen."

#: wpcf7-pdf-forms.php:1375
msgid "Embed Image"
msgstr "Bild Einbetten"

#: wpcf7-pdf-forms.php:1377
msgid "PDF file"
msgstr "PDF-Datei"

#: wpcf7-pdf-forms.php:1378
msgid "Page"
msgstr "Seite"

#: wpcf7-pdf-forms.php:1379
msgid "Select a region where the image needs to be embeded."
msgstr "Wählen Sie eine region, wo das Bild eingebettet werden muss."

#: wpcf7-pdf-forms.php:1387
msgid ""
"Have a question/comment/problem?  Feel free to use {a-href-forum}the support "
"forum{/a} and view {a-href-tutorial}the tutorial video{/a}."
msgstr ""
"Haben Sie eine Frage/Kommentar/problem? Fühlen Sie sich frei zu verwenden {a-"
"href-forum}das support-forum{/a} und Ansicht {a-href-tutorial}video-"
"tutorial{/a}."

#: wpcf7-pdf-forms.php:1391
msgid "Get Tags"
msgstr "Tags empfangen"

#: wpcf7-pdf-forms.php:1392
msgid "All PDFs"
msgstr "Alle PDF-Dateien"

#: wpcf7-pdf-forms.php:1393
msgid "Return to Form"
msgstr "Rückkehr zur Form"

#: wpcf7-pdf-forms.php:1398
msgid "Your CF7 plugin is too out of date, please upgrade."
msgstr "Ihre CF7 Plugin ist zu veraltet, bitte aktualisieren."

#. Plugin Name of the plugin/theme
msgid "PDF Forms Filler for Contact Form 7"
msgstr "PDF Forms Filler für Contact From 7"

#. Plugin URI of the plugin/theme
msgid "https://github.com/maximum-software/wpcf7-pdf-forms"
msgstr "https://github.com/maximum-software/wpcf7-pdf-forms"

#. Description of the plugin/theme
msgid ""
"Create Contact Form 7 forms from PDF forms.  Get PDF forms filled "
"automatically and attached to email messages upon form submission on your "
"website.  Embed images in PDF files.  Uses Pdf.Ninja API for working with "
"PDF files.  See tutorial video for a demo."
msgstr ""
"Erstellen Sie Kontakt-Formulars 7 Formulare in PDF-Formulare. Holen Sie sich "
"PDF-Formulare automatisch ausgefüllt und an E-Mail-Nachrichten beim senden "
"des Formulars auf Ihrer Webseite. Einbetten von Bildern in PDF-Dateien. "
"Verwendet Pdf.Ninja-API für das arbeiten mit PDF-Dateien. Siehe tutorial-"
"video für eine demo."

#. Author of the plugin/theme
msgid "Maximum.Software"
msgstr "Maximum.Software"

#. Author URI of the plugin/theme
msgid "https://maximum.software/"
msgstr "https://maximum.software/"

#~ msgid "Cannot create temporary PDF file"
#~ msgstr "Kann nicht temporäre PDF-Datei erstellen"

#~ msgid "Could not get a Pdf.Ninja API key."
#~ msgstr "Es konnte kein Pdf.Ninja API-Schlüssel erhalten."

#~ msgid "How-To"
#~ msgstr "Wie macht man"

#~ msgid ""
#~ "Attach a PDF file to your form and insert tags into your form that map to "
#~ "fields in the PDF file."
#~ msgstr ""
#~ "Bringen Sie eine PDF-Datei in das Formular und fügen Sie Tags in Ihre "
#~ "Form, die Felder in der PDF-Datei zuordnen."

#~ msgid "Invalid attachment ID"
#~ msgstr "Ungültige Befestigung ID"

#~ msgid "Invalid field"
#~ msgstr "Ungültiges Feld"
